name := "Url2vec"

version := "1.0"

scalaVersion := "2.11.3"

resolvers ++= Seq ("Local Maven Repository" at "file://" + Path.userHome.absolutePath + "/.m2/repository")
resolvers ++= Seq(Resolver.url("Local IVY2 Repository", url("file://"+Path.userHome.absolutePath+"/.ivy2/local"))(Resolver.ivyStylePatterns))

val jsoup = "org.jsoup" % "jsoup" % "1.8.3"
val jsoupDep = "ch.qos.logback" % "logback-classic" % "1.1.3"
val mapDb = "org.mapdb" % "mapdb" % "1.0.6"
val json4sNative = "org.json4s" %% "json4s-native" % "3.3.0"
val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % "3.4.0"
val scalatest = "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
val hylien = "it.datatoknowledge" %% "hylien" % "1.0.0"
val scalaj = "org.scalaj" %% "scalaj-http" % "2.3.0"

libraryDependencies ++= Seq(jsoup, scalaLogging, json4sNative, jsoupDep, scalatest, mapDb, hylien, scalaj).map(_.exclude("org.slf4j", "slf4j-simple"))

assemblyOption in assembly :=
  (assemblyOption in assembly).value.copy(includeScala = true)

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

