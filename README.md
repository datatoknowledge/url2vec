# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Dependencies ###

- Java 8

- Scala > 2.10

- Fast (branch SPARSE_DATASTRUCTURE), ListFlattener, WebPageTraverserWrapper

- Python

### What is this repository for?

It consists in two main parts: **Randon walk generation** and **url2vec**. 
####1. Random walk generation
The system generates seequences of urls in three different ways:
#####1.1 Random Walk
Given a graph and two integers L and D, the system generates L random walks of size D. For each random walk a node n_i is randomly chosen from the graph that represents the root of the walk and at each iteration j (0 < j < D), the walk is extended with a randomly chosen adjacent node.

#####1.2 Random Walk from homepage
Given an URL and two integers L and D, the system generates L walks of size D. Each walk starts from the given URL(homepage) and at each iteration j (0 < j < D), the walk is extended with a randomly chosen outlink in the last node.
#####1.3 Random walk with Lists
Given an URL and two integers L and D, the system generates L walks of size D. Each walk starts from the given URL(homepage) and at each iteration j (0 < j < D), the walk is extended with a randomly chosen outlink contained in the lists of the last node.

####2. Url2Vec
It is used word2vec algorithm to extract for each node a feature vector, that will be used for clustering purposes.

### How do I get set up? ###

Remember to install the local dependencies [webpagetraverserwrapper]() and [listflattener] ()

### Mains to run ###

####1. RandomWalkMain.
**Input:** 	
`website` : url website (e.g. http://cs.illinois.edu/) that HAVE TO END WITH "/"

`depth_breadth_search` : max depth of breadth search (used to extract url seeds)

`depth` : depth of the database (e.g 100000)

`length sequences` : max length of the generated sequences (e.g. 5)

`toConvert` : if true will be returned the converted sequences dataset

> Example input arguments:

> https://cs.illinois.edu/ 10 100000 5 true

**Output:** 
Folder containing the following files:

* *sequences.txt* contains sequences of urls 

	If `toConvert` is true:

	* *sequencesIDs.txt* contains sequences of codes in the format (u_i)
	* *sequencesMapUrl.txt* is a map that associate to each url a code

to run the jar:

```
#!bash

java -cp Url2vec.jar datasetGenerator.randomWalk.RandomWalkMain <website> <depth_breadth_search> <depth> <length sequences> <toConvert>
```

####2. RandomWalkFromHomepageMain####
**Input: **

`website` : url website (e.g. http://cs.illinois.edu/)

`depth` : depth of the database (e.g 100000)

`length sequences` : max length of the generated sequences (e.g. 5)

`toConvert` : if true will be returned the converted sequences dataset 

> Example input arguments:

> https://cs.illinois.edu/ 100000 5 true

**Output:** 
Same output of RandomWalkMain

to run the jar:

```
#!bash

java -cp Url2vec.jar datasetGenerator.randomWalkFromHomepage.RandomWalkFromHomepageMain <website> <depth> <length sequences> <toConvert>
```

####3. RandomWalkListsMain####
**Input: **

`website` : url website (e.g. http://cs.illinois.edu/)

`depth` : depth of the database (e.g 100000)

`length` : max length of the generated sequences (e.g. 5)

`toConvert` : if true will be returned the converted sequences dataset

`ip:port` : ip of web service to download web pages (e.g. 193.204.187.132:15000)

> Example input arguments:

> https://cs.illinois.edu/ 100000 5 true 193.204.187.132:15000

**Output:** 
Same output of RandomWalkMain

to run the jar: 

```
#!bash

java -cp Url2vec.jar datasetGenerator.randomWalkWithLists.RandomWalkListsMain <website> <depth> <length sequences> <toConvert> <ip:port>
```


### Problem to fix