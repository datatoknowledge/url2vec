package graphgenerator.withoutConstraints

import java.io.{BufferedWriter, File, FileWriter, PrintWriter}
import java.net.URL
import java.nio.file.{Files, Paths}
import java.util.Date

import com.typesafe.scalalogging.LazyLogging
import graphgenerator.Model._
import graphgenerator.Writers.{EdgesWriter, UrlMapWriter, VertexWriter}
import graphgenerator.{Model, UrlAnalyzer}
import org.jsoup.Jsoup

import scala.collection.JavaConverters._
import scala.io.Source
import scala.util.{Failure, Random, Success, Try}
import scalaj.http.{Http, HttpOptions}

/**
  * Created by fabiana on 1/22/16.
  */
class GraphGenerator(homepage: String, lengthText: Int, vertexFile: String, edgeFile: String, mapFile: String) extends LazyLogging {

  var countId = 3
  //associate to each url a code
  var urlMap = Map.empty[String, Int]
  val analyzedVertex = scala.collection.mutable.Set.empty[Model.Url]
  //Queue of url to analyze with its depth
  var queue = collection.mutable.Queue.empty[(String, Depth)]
  //Format: numeric_vertex_id [One Tab (\t)] words [separated by space].
  val vertexWriter = new VertexWriter(vertexFile)
  val urlMapWriter = new UrlMapWriter(mapFile)
  //Format: src_numeric_vertex_id [Tab \t] dst_numeric_vertex_id
  val edgeWriter = new EdgesWriter(edgeFile)
  val protocolHomepage = new URL(homepage).getProtocol
  val domainHomepage = UrlAnalyzer.getDomain(new URL(homepage).getHost)
  val minWait = 1000
  val maxWait = 5000
  var lastRequest = System.currentTimeMillis()
  val connTimeoutMs = 1000
  val readTimeoutMs = 10000



  /**
    * It uses scalaj to generate an HTTP request and return the web page's text
    * @param url to analyze
    * @return
    */
  private def getWebPageText(url: Url) : Try[String] = {
    Try {
      val html = Http(url)
        .option(HttpOptions.followRedirects(true))
        .timeout(connTimeoutMs = connTimeoutMs, readTimeoutMs = connTimeoutMs)
        .asString.body
      Jsoup.parse(html).body().text()
    }
  }

  /**
    * It uses scalaj to generate an HTTP request and return the web page's text
    * @param url to analyze
    * @return
    */
  private def getWebPageTextAndLinks(url: Url) : Try[(String, Set[String])] = {
    Try {
      val html = Http(url)
        .option(HttpOptions.followRedirects(true))
        .timeout(connTimeoutMs = connTimeoutMs, readTimeoutMs = connTimeoutMs)
        .asString.body
      val body = Jsoup.parse(html).body()
      val text = body.text()
      val javaUrl = new URL(url)
      val convertJavaURL = new PartialFunction[String, URL] {
        def apply(url: String) = new URL(url)
        def isDefinedAt(url: String) = Try(new URL(url)).isSuccess
      }
      val convertJavaRelativeURL =  new PartialFunction[ String, URL] {
        override def isDefinedAt(x: String): Boolean = Try(new URL(javaUrl, x)).isSuccess
        override def apply(v1: String): URL = new URL(javaUrl, v1)
      }

      val urls = body.select("a[href]")
        .asScala
        .map(link => link.attr("href"))
        .toList
        .filter(s => s.size > 0)
        .filter(UrlAnalyzer.isValid)
        .collect(convertJavaRelativeURL)
        .filter(u => UrlAnalyzer.checkDomain(domainHomepage, u, true ))
        .map{u =>(UrlAnalyzer.normalizeUrl(protocolHomepage, u), u) }
          .unzip._1
          .toSet
        //.partition(u => u.startsWith("http"))

//      //remove eventually email and noisy links
//      val updatedRelativeUrls = relativeUrls
//        .toSet
//        .filter(UrlAnalyzer.isValid)
//        .collect(convertJavaRelativeURL)
//        .map{u =>(UrlAnalyzer.normalizeUrl(protocolHomepage, u), u) }
//
//      val updatedAbsoluteUrls = absoluteUrls
//        .toSet
//        .filterNot(url => UrlAnalyzer.extensionToIgnore.exists(s => url.toLowerCase.endsWith(s)))
//        .collect(convertJavaURL)
//        .filter(url => UrlAnalyzer.checkDomain(domainHomepage, url.getHost, true))
//        .map(s => (UrlAnalyzer.normalizeUrl(protocolHomepage, s), s))
//      val (lists, _) = (updatedAbsoluteUrls ++ updatedRelativeUrls).filter(u => UrlAnalyzer.checkDomain(domainHomepage, u._2, true )).unzip

      (text, urls)
    }
  }

  private def generateWebPageWithoutLinks(url: Url) = {
    delay()
    val truncatedUrl = UrlAnalyzer.truncateUrl(url)
    val id = addUrlMap(truncatedUrl)
    val tryText = getWebPageText(url)

    tryText match {
      case Success(text) =>
        val normalizedUrl = UrlAnalyzer.normalizeUrl(protocolHomepage, new URL(url))
        val content =  convertContent(text)
        val truncatedUrl = UrlAnalyzer.truncateUrl(url)
        Some(WebPage(normalizedUrl, truncatedUrl, id, content, Set(), Set.empty[Link]))

      case Failure(ex) => logger.error(s"$url returns exception $ex")
        None
    }

  }

  def run(url: Url, maxDepth: Int): Unit = {

    queue.enqueue((url,0))
    val truncatedUrl = UrlAnalyzer.truncateUrl(url)
    analyzedVertex += truncatedUrl

    while(queue.nonEmpty) {

      val (headUrl, depth) = queue.dequeue()

      println("Analyzing url "+ headUrl +  " - depth "+ depth )
      val optionWebPage =
        if(depth == maxDepth -1)
          generateWebPageWithoutLinks(headUrl)
        else
          generateWebPage(headUrl)

      logger.info("Analyzing url "+ headUrl + " ( code: "+ urlMap.get(headUrl).get + " - depth"+ depth + " )" )

      optionWebPage match {

        case Some(webPage) =>
          vertexWriter(webPage)
          urlMapWriter(webPage)

          //outlinks of urls having max depth are not written
          if(depth < maxDepth -1){
            edgeWriter(webPage)

            val linksToAnalyze = webPage.links.filterNot(l => analyzedVertex.contains(l.truncatedUrl))
            analyzedVertex ++= linksToAnalyze.map(l => l.truncatedUrl)
            //linksToAnalyze.foreach(l => println(l.truncatedUrl))
            linksToAnalyze.foreach(l => queue.enqueue((l.url, depth + 1)))
          }

        case _ => logger.error(s"$headUrl returns some error")
      }

    }
    vertexWriter.close()
    edgeWriter.close()

  }

  private def isMaxDepth(depth:Int, maxDepth:Int): Boolean = {
    depth.equals(maxDepth)
  }


  /**
    * Given a url return a option of web page
    *
    * @param url
    * @return
    */
  private def generateWebPage(url: String): Option[WebPage] = {
    delay()
    val truncatedUrl = UrlAnalyzer.truncateUrl(url)
    val id = addUrlMap(truncatedUrl)
    //returned links are yet normalized (i.e. included url redirect transformation)
    val tryTextLinks = getWebPageTextAndLinks(url)

    tryTextLinks match {
      case Success((text, urls)) =>
        //val normalizedUrl = UrlAnalyzer.normalizeUrl(protocolHomepage, new URL(url))
        val content =  convertContent(text)
        val truncatedUrl = UrlAnalyzer.truncateUrl(url)
        val id = addUrlMap(truncatedUrl)
        val links = urls.map(u => Link(truncatedUrl= UrlAnalyzer.truncateUrl(u), url=u))
        val convertedLinks = convertLinks(links)
        Some(WebPage(url, truncatedUrl, id, content, convertedLinks._1.toSet, convertedLinks._2.toSet))

      case Failure(ex) => logger.error(s"$url returns exception $ex")
        None
    }
  }


  /**
  * add url to urlMap and return its id
  * @param link
  * @return
  */
  private def addUrlMap(link: Link): Int = {

    urlMap.isDefinedAt(link.truncatedUrl) match {
      case true =>
        urlMap.get(link.truncatedUrl).get
      case false =>
        val id = countId
        urlMap = urlMap + (link.truncatedUrl -> countId)
        countId +=1
        id
    }

  }

  private def addUrlMap(truncatedUrl:String): Int = {
    urlMap.isDefinedAt(truncatedUrl) match {
      case true =>
        urlMap.get(truncatedUrl).get
      case false =>
        val id = countId
        urlMap = urlMap + (truncatedUrl -> countId)
        countId +=1
        id
    }
  }

  /**
    *Used before any HTTP request avoid to be banned by websites
    *
    */
  private def delay(): Unit = {
    val currentTime = System.currentTimeMillis()
    val r = new Random()
    val randomWait = minWait + r.nextInt( maxWait - minWait + 1 )
    val diffTime = currentTime - lastRequest

    val tot = randomWait-diffTime
    if(tot>0){
      println(s"\tAwait $tot millisec; last request was $diffTime msec ago")
      Thread.sleep(tot)
    }
    lastRequest = currentTime
  }

  /**
    * Converts a list of urls in a list of ID
    *
    * @param links
    * @return
    */
  private def convertLinks(links: Set[Link]) = links.map(l => (addUrlMap(l), l)).unzip

  /**
    * Convert a text in tokens splitted with spaces
    *
    * @param text
    * @return
    */
  private def convertContent(text: String): String = {
    val splittedText = text.replaceAll("[^a-zA-Z ]", " ").toLowerCase().split("\\s+").mkString(" ")
    truncateText(splittedText)
  }

  /**
    * If the web page has 1 word then duplicate the word, else truncate the words to maxLenght constraint
    *
    * @param text
    * @return
    */
  private def truncateText(text: String): String = {
    val tokens = text.split(" ")

    if (tokens.length == 0) {
      "nothing nothing"
    }
    else if (tokens.length == 1) {
      tokens + " " + tokens
    } else if (tokens.length <= lengthText) {
      tokens.mkString(" ")
    } else {
      tokens.slice(0, lengthText).mkString(" ")
    }
  }


  def printUrlMap(path: String): Unit = {
    val file = new File(path)
    val bw = new BufferedWriter(new FileWriter(file))
    urlMap.keys.foreach(k => bw.write(s"$k, ${urlMap.get(k).get}\n"))
    bw.close()
  }

  def pruneInexistentEdges(fileEdges: String, fileVertex: String, fileOutput: String): Unit = {
    val vertex: List[Int] = Source.fromFile(fileVertex).getLines().map( line => line.split("\t")(0).toInt).toList
    val file = new File(fileOutput)
    val out = new PrintWriter(file, "UTF-8")

    Source.fromFile(fileEdges).getLines().foreach{ line =>
      val outlink = line.split("\t")(1).toInt
      vertex.contains(outlink) match {
        case false =>
          logger.info("Line "+ line + " removed!")

        case true =>
          out.println(line)
      }
    }

    out.close()
  }

  def printTruncatedUrls(path: String): Unit = {
    val file = new File(path)
    val bw = new BufferedWriter(new FileWriter(file))

    urlMap.keys.foreach(k => bw.write(s"$k\t${urlMap.get(k).get}\n"))
    bw.close()
  }


}



object GraphGeneratorMain {


  def main(args: Array[String]): Unit = {
    args match{
      case Array(homepage, lengthText, maxDepth) =>
        val outputPath = Paths.get(new URL(homepage).getHost.replace("www.", "") + ".NoConstraint.words" + lengthText+".depth"+maxDepth)

        if (Files.exists(outputPath)) {
          Console.err.println("remove the directory " + outputPath.toAbsolutePath + " before running for " + homepage)

        } else {
          Files.createDirectory(outputPath)
          val vertexPath = s"${outputPath.toAbsolutePath}/vertex.txt"
          val edgesPath = s"${outputPath.toAbsolutePath}/edgesToPrune.txt"
          val urlMapPath = s"${outputPath.toAbsolutePath}/urlsMap.txt"
          val truncatedUrls = s"${outputPath.toAbsolutePath}/truncatedUrls.txt"
          val graphGenerator = new GraphGenerator(homepage, lengthText.toInt, vertexPath, edgesPath, urlMapPath)
          val startTime = new Date().getTime

          println("Dataset generation starting at "+ startTime)
          graphGenerator.run(homepage, maxDepth.toInt)
          val endTime = new Date().getTime
          val totSec = (endTime-startTime)/1000
          println("Dataset generation completed in " + totSec + " seconds")

          val prunedEdgesPath = s"${outputPath.toAbsolutePath}/edges.txt"
          println("Start pruning edges to inexistent outlinks")
          val startTimePruning = new Date().getTime
          graphGenerator.pruneInexistentEdges(edgesPath, vertexPath, prunedEdgesPath)
          val endTimePruning = new Date().getTime
          val totSecPruning = (endTimePruning-startTimePruning)/1000
          println("End pruning edges to inexistent outlinks in "+totSecPruning+" seconds " )

          graphGenerator.printTruncatedUrls(truncatedUrls)

          new File(edgesPath).delete()





        }

      case _ => Console.err.println(s"wrong parameters for: ${args.mkString(" ")}")
        //Console.err.println("Insert correct arguments: <website> <depth> <search_only_in_same_domain> <GuiIsPresent>(ie http://www.uniba.it 5 true true)")
        val string = """to run the jar do: java -cp webgraphgenerator.jar datasetGeneratorMain <website> <length_content> <max_depth>
                       | where:
                       | <website> : url website (e.g. http://cs.illinois.edu/)
                       | <length_content> : content size of each web page (e.g 100)
                       | <max_depth> : max depth to visit (e.g. 5)
                     """

        Console.err.println(string)
    }



  }
}
