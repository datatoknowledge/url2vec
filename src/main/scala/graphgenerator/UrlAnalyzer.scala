package graphgenerator

import java.net.URL

import com.typesafe.scalalogging.LazyLogging
import org.mapdb.{DB, DBMaker}
import java.util.Map
import scala.util.{Failure, Success, Try}
import scalaj.http.{Http, HttpOptions}

/**
  * Created by fabiana on 1/25/16.
  */
object UrlAnalyzer extends LazyLogging{
  val dbUrl: DB = DBMaker.newMemoryDB().transactionDisable().cacheSize(1000000).make()
  val redirectUrls: Map[String, String] = dbUrl.createTreeMap("redirectedUrl").valuesOutsideNodesEnable().make()

  def getDomain(host: String ) = if (host.startsWith("www."))  host.substring(4) else host

  /**
    *
    * @param domainHomepage
    * @param domainItem
    * @param typeSearch
    * @return
    */
  def checkDomain(domainHomepage: Model.Url, domainItem: Model.Url, typeSearch: Boolean): Boolean = {
    //FIXME update this method
    if (!typeSearch)
      true
    val domain = getDomain(domainItem)
    domain.equals(domainHomepage)
  }

  /**
    *
    * @param domainHomepage
    * @param domainItem
    * @param typeSearch
    * @return
    */
  def checkDomain(domainHomepage: Model.Url, domainItem: URL, typeSearch: Boolean): Boolean = {
    //FIXME update this method
    if (!typeSearch)
      true
    val domain = getDomain(domainItem.getHost)
    domain.equals(domainHomepage)
  }

  def extensionToIgnore = List(".xml", ".js", ".css", ".less", ".png", ".jpg", ".jpeg",".ps",".pl", "javascript:null(0);",
    ".gif", ".pdf", ".doc", ".txt", ".zip", ".mp3", ".rar", ".exe", ".wmv", ".doc", ".avi", ".ppt", ".mpg", ".gz", ".ova",
    ".mpeg", ".tif", ".wav", ".mov", ".psd", ".ai", ".xls", ".mp4", ".m4a", ".swf", ".dat", ".dmg", ".key",
    ".iso", ".flv", ".m4v", ".torrent", ".java", ".pdf", ".epub", ".docx", ".cvf", ".xsls", ".docx", ".pptx",
    ".xml", ".bib")

  /**
    *
    * @param url
    * @return true if the url contains @
    */
  def isEmail(url: String): Boolean = url.contains("@")

  /**
    *
    * @param url
    * @return false if the url is not a html page (i.e. the url extension is contained in the extensionToIgnore List) or url is an email
    */
  def isValid(url: String): Boolean = !isEmail(url) & !extensionToIgnore.exists(s => url.toLowerCase.endsWith(s))

  /**
    * Given and url, change its protocol with homepage's protocol and if it redirects to another url x return x
    *
    * @param protocolHomepage
    * @param url
    * @return
    */
  def normalizeUrl(protocolHomepage: String, url: URL): String = {
    val tUrl = truncateUrl(url.toExternalForm)
    if (redirectUrls.containsKey(tUrl))
      redirectUrls.get(tUrl)
    else {
      val defaultUrl = protocolHomepage + "://" + url.getHost + url.getPath

      val tryReq = Try {
        Http(url.toExternalForm)
          .option(HttpOptions.followRedirects(false))
          .timeout(connTimeoutMs = 10000, readTimeoutMs = 30000)
          .asString}

      tryReq match {
        case Failure(ex) => logger.error(s"URL: $url ${ex.getMessage}")
          defaultUrl

        case Success(req) =>
          val tryLocation = Try(req.headers.get("Location").headOption.map(u => new URL(url, u.head)))

          tryLocation match {
            case Success(location) =>
              logger.info(s"URL ${url.toExternalForm} is redirected to URL $location")
              val javaUrl = location.getOrElse(url)
              val newUrl = protocolHomepage + "://" + javaUrl.getHost + javaUrl.getPath
              redirectUrls.put(tUrl, newUrl)
              newUrl

            case Failure(ex) =>
              val status = req.headers.get("status").headOption
              val location = req.headers.get("Location").headOption
              //val url_location = new URL(url, location)
              logger.error(s"URL ${url.toExternalForm} has a redirect to $location having status $status")
              redirectUrls.put(tUrl, defaultUrl)
              defaultUrl
          }
      }
    }
  }

  def truncateUrl(url: Model.Url): String = {
    val truncateEnd = url match {
      case string: String if string.endsWith("/index.html") =>
        string.dropRight(11)
      case string: String if string.endsWith(".html") =>
        string.dropRight(5)
      case string: String if string.endsWith("/") =>
        string.dropRight(1)
      case string: String if string.endsWith("/index.jsp") =>
        string.dropRight(10)

      case _ =>
        url
    }
    truncateEnd match {
      case string: String if string.startsWith("http://www.") =>
        string.drop(11)
      case string: String if string.startsWith("http://") =>
        string.drop(7)
      case string: String if string.startsWith("https://www.") =>
        string.drop(12)
      case string: String if string.startsWith("https://") =>
        string.drop(8)
      case string => string
    }
  }


}
