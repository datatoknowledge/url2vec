package graphgenerator

import java.io.{PrintWriter, File}

import graphgenerator.Model.WebPage

/**
  * Created by fabiana on 1/25/16.
  */
object Writers {

  class UrlMapWriter(filePath: String) extends Function1[WebPage, Unit] {
    val file = new File(filePath)
    val out = new PrintWriter(file, "UTF-8")
    def apply(webPage: WebPage): Unit = {
      out.println(getVertexStr(webPage))
      out.flush()
    }
    def close(): Unit =
      out.close()

    def getVertexStr(webPage: WebPage) = {
      webPage.url + "\t" + webPage.id
    }
  }

  class VertexWriter(filePath: String) extends Function1[WebPage, Unit] {
    val file = new File(filePath)
    val out = new PrintWriter(file, "UTF-8")
    def apply(webPage: WebPage): Unit = {
      out.println(getVertexStr(webPage))
      out.flush()
    }
    def close(): Unit =
      out.close()

    def getVertexStr(webPage: WebPage) = {
      webPage.id + "\t" + webPage.content
    }
  }

  class EdgesWriter(filePath: String) extends Function1[WebPage, Unit] {

    val file = new File(filePath)
    val out = new PrintWriter(file, "UTF-8")

    def apply(webPage: WebPage): Unit = {
      webPage.linkIds.foreach(l => out.println(webPage.id + "\t" + l))
      out.flush()
    }

    def close(): Unit =
      out.close()

  }
}
