package sequenceGenerator

import java.io.{BufferedWriter, File, FileWriter}

import scala.io.Source
import scala.util.Random

/**
  * Created by fabiana on 6/12/16.
  */
class SeedsGenerator(seedsFile: String, edgesFile: String, lengthRandomWalk: Int, databaseLength: Int){

  val seeds = Source.fromFile(seedsFile).getLines().map(l => l.split("\t")(1).toInt).toList
  val outlinkMap = readOutlinksMap
  val inlinksMap = getInlinks()

  private def readOutlinksMap(): Map[Int, List[Int]] = {
    scala.io.Source.fromFile(edgesFile).getLines
      .map{line =>
        val tokens = line.split("\t")
        (tokens(0), tokens(1))}
      .toList.groupBy(a=> a._1)
      .map { case(key, list) =>
        val outlinks = list.map(_._2.toInt)
        (key.toInt, outlinks)
      }
  }

  def getInlinks() : Map[Int, List[Int]] = {
    scala.io.Source.fromFile(edgesFile).getLines
      .map{line =>
        val tokens = line.split("\t")
        (tokens(1), tokens(0))}
      .filter(x => x._1 != x._2)
      .toList.groupBy(a=> a._1)
      .map { case(key, list) =>
        val inlinks = list.map(_._2.toInt)
        (key.toInt, inlinks)
      }
  }

  def run() = {


    val parent = new File(edgesFile).getParent
    val output = parent + "/sequenceIDs.txt"
    val file = new File(output)
    val pw = new BufferedWriter(new FileWriter(file))


//    val sequences = Range(0, databaseLength).map{ i =>
//      val rand = new Random()
//      val random_index = rand.nextInt(seeds.size)
//      val source = seeds(random_index)
//      val sequence = extendSequence(List(source))
//      extendSequence(List(source)).mkString(" ")
//    }

    val seedIterations = if(databaseLength<seeds.size)
      1
    else
      databaseLength / seeds.size


    val sequences = for{
      seed <- seeds
      j <- Range(0, seedIterations)
    } yield extendSequenceWithInlinks(List(seed)).mkString(" ")

    sequences.foreach{s => pw.write(s+"\n")}
    pw.flush()

    println("sequences generation finished. Stored in "+output)
    pw.close()
  }

  def extendSequence(sequence: List[Int]): List[Int] = {
    if(sequence.length == lengthRandomWalk){
      sequence.reverse
    }else{
      val lastItem = sequence.head

      outlinkMap.get(lastItem) match {
        case Some(outlinks) =>
          val rand = new Random()
          val random_index = rand.nextInt(outlinks.length)
          val randomElement = outlinks(random_index)
          val newSeq = randomElement +:sequence
          extendSequence(newSeq)
        case None => sequence.reverse
      }
    }

  }

  def extendSequenceWithInlinks(sequence: List[Int]): List[Int] = {
    if(sequence.length == lengthRandomWalk){
      sequence.reverse
    }else{
      val lastItem = sequence.head

      inlinksMap.get(lastItem) match {
        case Some(inlinks) =>
          val rand = new Random()
          val random_index = rand.nextInt(inlinks.length)
          val randomElement = inlinks(random_index)
          val newSeq = randomElement +:sequence
          extendSequence(newSeq)
        case None => sequence.reverse
      }
    }

  }

}

object SeedsGeneratorMain {
  def main(args: Array[String]) {
    args match {
      case Array(seedsFile: String, edgesFile: String, numRandomWalks: String, databaseLength: String) =>
        val sg = new SeedsGenerator(seedsFile, edgesFile, numRandomWalks.toInt, databaseLength.toInt)
        sg.run()
      case _ => println("mah")
    }
  }
}