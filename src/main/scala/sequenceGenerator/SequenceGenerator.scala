package sequenceGenerator

import java.io.{File, PrintWriter}

import scala.util.Random

/**
  * Created by fabiana on 1/25/16.
  */
@Deprecated
class SequenceGenerator (edgesFile: String, numRandomWalks: Int, lengthRandomWalk: Int){



  val outlinkMap = readOutlinksMap

  private def readOutlinksMap(): Map[Int, List[Int]] = {
    scala.io.Source.fromFile(edgesFile).getLines
      .map{line =>
        val tokens = line.split("\t")
        (tokens(0), tokens(1))}
      .toList.groupBy(a=> a._1)
      .map { case(key, list) =>
        val outlinks = list.map(_._2.toInt)
        (key.toInt, outlinks)
      }
  }

  def runFromHomepage(homepageId: Int): Unit = {
    val output = new File(edgesFile).getParent + "/sequenceIDsFromHomepage.txt"
    val file = new File(output)
    val out = new PrintWriter(file, "UTF-8")
    Range(0, numRandomWalks).foreach{ i =>
      val sequence = extendSequence(List(homepageId))
      out.println(sequence.mkString(" "))
    }
    out.close()
  }

  def run(): Unit = {
    val output = new File(edgesFile).getParent + "/sequenceIDs.txt"
    val file = new File(output)
    val out = new PrintWriter(file, "UTF-8")
    val startingNodes = outlinkMap.keys.toList

    Range(0, numRandomWalks).foreach{ i =>
      val rand = new Random()
      val random_index = rand.nextInt(startingNodes.size)
      val source = startingNodes(random_index)
      val sequence = extendSequence(List(source))
      out.println(printSequence(sequence))
    }
    out.close()

  }

  private def printSequence(list: List[Int]): String = {
    list.map(el => el + " -1 ").mkString("")+ "-2"
  }

  def extendSequence(sequence: List[Int]): List[Int] = {
    if(sequence.length== lengthRandomWalk){
        sequence.reverse
    }else{
      val lastItem = sequence.head

      outlinkMap.get(lastItem) match {
        case Some(outlinks) =>
          val rand = new Random()
          val random_index = rand.nextInt(outlinks.length)
          val randomElement = outlinks(random_index)
          val newSeq = randomElement +:sequence
          extendSequence(newSeq)
        case None => sequence.reverse
      }
    }

  }

}
@Deprecated
object SequenceGeneratorMain {
  def main(args: Array[String]): Unit = {
    args match {
      case Array(edgeFile, numRandomWalks, lengthRandomWalk, homepage) =>
        val sq = new SequenceGenerator(edgeFile, numRandomWalks.toInt, lengthRandomWalk.toInt)
        sq.runFromHomepage(homepage.toInt)

      case Array(edgeFile, numRandomWalks, lengthRandomWalk) =>
        val sq = new SequenceGenerator(edgeFile, numRandomWalks.toInt, lengthRandomWalk.toInt)
        sq.run()

      case _   =>
        Console.err.println(s"wrong parameters for: ${args.mkString(" ")}")
        //Console.err.println("Insert correct arguments: <website> <depth> <search_only_in_same_domain> <GuiIsPresent>(ie http://www.uniba.it 5 true true)")
        val string = """to run the jar do: java -cp name.jar sequenceGenerator <edgeFile> <numRandomWalks> <lengthRandomWalk> <homepage> (OPTIONAL)
                       | where:
                       | <edgeFile> : file containing edges (each line is **id tab id**)
                       | <numRandomWalks> : database size (e.g. 100000)
                       | <lengthRandomWalk> :length of each random walker (e.g. 5)
                       | <homepage>: optional, id of homepage for randomwalk generation from homepage (e.g. 1)
                     """

        Console.err.println(string)

    }
  }
}
