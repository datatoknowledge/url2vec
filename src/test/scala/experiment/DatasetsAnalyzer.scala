package experiment

import scala.io.Source

/**
  * Created by fabiana on 6/3/16.
  */
class DatasetsAnalyzer(file1: String, file2:String, separator: String) {
  val noListConstraint = Source.fromFile(file1).getLines().map(l => l.split(separator)(0)).toList
  val listConstraint = Source.fromFile(file2).getLines().map(l => l.split("\t")(0)).toList
  val intersect = noListConstraint.intersect(listConstraint)


  def diff1() = listConstraint.diff(noListConstraint)

  def diff2() = noListConstraint.diff(listConstraint)

}

object Main{
  def main(args: Array[String]) {
    //val noListConstraint = "/home/fabiana/git/Url2vec/lsa.umich.edu.NoConstraint.words100.depth3/truncatedUrls.txt"
    //val listConstraint = "/home/fabiana/git/Url2vec/lsa.umich.edu.ListConstraint.words100.depth3/truncatedUrls.txt"

    val noListConstraint =  "/home/fabiana/git/Url2vec/cs.ox.ac.uk.NoConstraint.words100.depth3/truncatedUrls.txt"
    val listConstraint = "/home/fabiana/git/Url2vec/cs.ox.ac.uk.ListConstraint.words100.depth3/truncatedUrls.txt"
    val da = new DatasetsAnalyzer(noListConstraint, listConstraint, ", ")
    println(s"Size noListConstraint: ${da.noListConstraint.size}")
    println(s"Size listConstraint: ${da.listConstraint.size}")
    println(s"Size Intersection ${da.intersect.size}")
    println(s"diff listConstraint/noListConstraint ${da.diff1().size}")
    da.diff1().foreach(e => println(s"\t$e"))
    println(s"diff noListConstraint/ListConstraint ${da.diff2().size}")
    da.diff2().foreach(e => println(s"\t$e"))
  }
}