package experiment

import java.io.File

import com.machinepublishers.jbrowserdriver.{JBrowserDriver, Settings, Timezone}
import org.apache.commons.io.FileUtils
import org.openqa.selenium.{Dimension, OutputType}

/**
  * Created by fabiana on 6/3/16.
  */
object Screenshoot {

  def main(args: Array[String]) {
    val settings = Settings.builder()
      .timezone(Timezone.EUROPE_ROME)
      .headless(true)
      .screen(new Dimension(1920, 1080))
      .cache(true)
      .quickRender(true)
      .ajaxWait(300)
      .build()

    val  driver = new JBrowserDriver(settings)
    driver.get("http://www.cs.ox.ac.uk")
    val scrFile = driver.getScreenshotAs(OutputType.FILE)
    println("Screenshot in progress")
    // Now you can do whatever you need to do with it, for example copy somewhere
    FileUtils.copyFile(scrFile, new File("screenshot.png"))
    println("Screenshot done!")
    driver.close()
  }
}
