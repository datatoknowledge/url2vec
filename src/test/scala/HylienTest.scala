import eu.unicredit.web.hylien.VisualHyLiEn

/**
  * Created by fabiana on 6/3/16.
  */
object HylienTest {
  def main(args: Array[String]) {
    val hylien = new VisualHyLiEn()
    val lists = hylien.extract("http://cs.stanford.edu/directory/research-staff")

    lists.foreach{list =>
      println("***********************************")
      list.elements.foreach{we =>
        println(s"Text ${we.text}")
        println(s"Links ${we.urls}")
      }
    }

    hylien.close()
  }

}
